# teste-QA

## Começando

Para executar o projeto, será necessário instalar os seguintes programas:

- [Python 3.8](https://www.python.org/downloads/)

- [Pipenv - Gerenciador de dependências](https://pypi.org/project/pipenv/)
- [Google Chrome](https://www.google.pt/intl/pt-PT/chrome/?brand=ISCS&gclid=CjwKCAiA8Jf-BRB-EiwAWDtEGslReK2rZGOrkLtvHFnkFGL20St5KMR_BcHZNxEkbqoVer1RbmurNRoC2zgQAvD_BwE&gclsrc=aw.ds)


### Execução

Para iniciar os script de teste precisamos primeiro instalar as dependências do projeto:

```shell
pipenv install
```

O comando irá instalar as dependências do projeto e criará um virtual environment para 
a execução do projeto, após a instalacão vamos executar o comando para ativar esse environment 

```shell
pipenv shell
```
depois basta executar o behave para executar os testes

```shell
behave
```