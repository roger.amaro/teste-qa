from os import path

import ipdb
from ipdb import spost_mortem
from selenium import webdriver


def before_all(context):
    context.web = webdriver.Chrome(executable_path=path.abspath("chromedriver"))
    context.web.get("http://automationpractice.com/index.php?")


def after_step(context, step):
    if context.config.userdata.getbool("debug") and step.status == "failed":
        ipdb.sset_trace()
        spost_mortem(step.exc_traceback)


def after_all(context):
    context.web.quit()
