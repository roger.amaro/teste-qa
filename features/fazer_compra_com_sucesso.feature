#language: pt
Funcionalidade: Realizar uma compra com sucesso
  Afim de comprar produtos
  os clientes devem ser capazes
  de realizar uma compra com sucesso pelo site


  Cenario: adicionar um produto qualquer da loja na sacola de compras
    Quando o usuário adicionar qualquer produto a sacola
    Entao o usuário verá um resumo da sua sacola de compras

  Cenario: Prosseguir para o checkout
    Dado que o cliente tenha pelo menos um produto no carrinho
    Quando o cliente clicar no botão "Proceed to checkout"
    Entao o cliente irá para a pagina de checkout

  Cenario: Validar os itens do carrinho
    Dado que o cliente esta na página de checkout
    Então o produto escolhido por ele deve estar no checkout
    E o valor do frete deve ser visível
    E a quantidade total de produtos deve ser visível
    E o valor total da compra deve ser visível
    E o valor da taxa deve ser visível
    E o botão "proceed to checkout" deve ser visível

  Cenario: Prosseguir para sign
      Dado que o cliente esta na página de checkout e o botão proceed to checkout esta visível
      Quando o cliente pressiona o botão "proceed to checkout"
      Entao o cliente deve avançar para a etapa sign

  Cenario: Criar uma conta
    Dado que o cliente esta na etapa "sign-in"
    Quando o cliente preenche o campo "email address" com um email válido
    E clica no botão "create an account"
    Então o cliente verá a tela de cadastro com o formulário de cadastro

  Cenario: Preencher formulário e validar endereço
    Dado que o formulário de cadastro visível
    Quando o cliente preencher todos os dados do formulário corretamente
    E clicar no botão "Register"
    Entao a página de validação do endereço é visível
    E os dados sao verificados


  Cenario: prosseguir para os termos de serviço de entrega
    Dado que o cliente esta na pagina de validação do endereço
    Quando quando o botão "proceed to checkout" e pressionado
    Então O usuário e direcionado para a etapa de termos de serviço de entrega

  Cenario: Aceitar os termos de serviço da entrega
    Dado que o cliente esta vendo a caixa de selecção dos termos de serviço da entrega
    Quando clicar na caixa de seleção
    Então a caixa de seleção deve estar marcada

  Cenario: prosseguir para o pagamento
    Dado que o cliente esteja no etapa de termos de serviço de entrega
    E o botão de "proceed to checkout" esta visível
    Quando o cliente clica no botão "proceed to checkout"
    Entao o cliente deve avançar para a etapa de pagamento

  Cenario: Validar o valor total da compra
   Dado que o cliente esta visualizando a etapa de pagamento
   Entao deverá estar visível para o cliente o valor total da compra

  Cenario: Selecionar método de pagamento
    Dado que o cliente esta visualizando a etapa de pagamento e os 2 métodos de pagamentos disponíveis
    Quando o cliente clica em um método
    Então o cliente é redirecionado para a confirmação do método

  Cenario: Confirmar o método de pagamento  e validar se a compra foi finalizada com sucesso
    Dado que o cliente esta na página de confirmação do método de pagamento
    Quando o cliente clica no botão "I confirm my order"
    Então o cliente visualiza a pagina de confirmação do pedido
    E a mensagem "Your order on My Store is complete." estará visível


