from behave import then, given
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait as wait


@given(u'que o cliente esta na página de checkout')
def step_impl(context):
    page_title = wait(context.web, 20).until(EC.presence_of_element_located((By.CLASS_NAME, "navigation_page")))
    assert page_title.text == "Your shopping cart"
    assert page_title.is_displayed() is True
    assert context.web.current_url == "http://automationpractice.com/index.php?controller=order"


@then(u'o produto escolhido por ele deve estar no checkout')
def step_impl(context):
    assert context.web.find_element_by_class_name("cart_description").is_displayed() is True
    assert context.web.find_element_by_class_name("cart_product").is_displayed() is True


@then(u'o valor do frete deve ser visível')
def step_impl(context):
    cart_total_delivery = context.web.find_element_by_class_name("cart_total_delivery")
    assert cart_total_delivery.is_displayed() is True
    total_shipping = cart_total_delivery.find_element_by_id("total_shipping")
    assert total_shipping.is_displayed() is True
    context.total_shipping = float(total_shipping.text[1:-1])
    assert total_shipping.text[0] == "$"
    assert context.total_shipping == 2.0


@then(u'a quantidade total de produtos deve ser visível')
def step_impl(context):
    summary_products_quantity = context.web.find_element_by_id("summary_products_quantity")
    assert summary_products_quantity.is_displayed() is True
    assert summary_products_quantity.text == '1 Product'


@then(u'o valor total da compra deve ser visível')
def step_impl(context):
    total_price = context.web.find_element_by_id("total_price")
    total_product = context.web.find_element_by_id("total_product")

    assert total_price.is_displayed() is True
    assert total_product.is_displayed() is True

    total_price = float(total_price.text[1:-1])
    total_product = float(total_product.text[1:-1])

    assert round(total_price, 1) == round(total_product,1) + round(context.total_shipping,1)


@then(u'o valor da taxa deve ser visível')
def step_impl(context):
    cart_total_tax = context.web.find_element_by_class_name("cart_total_tax")
    assert cart_total_tax.is_displayed()


@then(u'o botão "proceed to checkout" deve ser visível')
def step_impl(context):
    btn_title = 'Proceed to checkout'
    btn_proceed_to_checkout = context.web.find_element_by_link_text(btn_title)
    assert btn_proceed_to_checkout.is_displayed() is True
