from behave import given, when, then
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait as wait


@given(u'que o cliente tenha pelo menos um produto no carrinho')
def step_impl(context):
    qtd_cart_text = '1'
    element = context.web.find_element_by_class_name("ajax_cart_quantity")
    qtd_in_cart = ''.join(c for c in element.text if c.isdigit())
    assert qtd_cart_text >= element.text, f"a quantidade de produtos no carrinho atualmente deveria ser '{qtd_cart_text}' mas foram " \
                                          f"encontrados {qtd_in_cart} produtos"


@when(u'o cliente clicar no botão "Proceed to checkout"')
def step_impl(context):
    btn_title = 'Proceed to checkout'
    btn_proceed_to_checkout = wait(context.web, 20).until(
        EC.element_to_be_clickable((By.CSS_SELECTOR, f"[title^='{btn_title}']")))
    assert btn_title == btn_proceed_to_checkout.text, f'o titulo do botao deveria ser {btn_title} mas e \
                        {btn_proceed_to_checkout.text}'
    assert btn_proceed_to_checkout.is_displayed() is True, "o botao 'continue shopping' deveria ter sido renderizado"
    context.btn_proceed_to_checkout = btn_proceed_to_checkout


@then(u'o cliente irá para a pagina de checkout')
def step_impl(context):
    context.btn_proceed_to_checkout.click()
    page_title = wait(context.web, 20).until(EC.presence_of_element_located((By.CLASS_NAME, "navigation_page")))
    assert page_title.text == "Your shopping cart"
    assert page_title.is_displayed() is True
    assert context.web.current_url == "http://automationpractice.com/index.php?controller=order"


