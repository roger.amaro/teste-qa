from behave import given, when, then
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait as wait


@given(u'que o cliente esta na página de checkout e o botão proceed to checkout esta visível')
def step_impl(context):
    btn_title = 'Proceed to checkout'
    btn_proceed_to_checkout = context.web.find_element_by_link_text(btn_title)
    assert btn_proceed_to_checkout.is_displayed() is True
    context.btn_proceed_to_checkout = btn_proceed_to_checkout


@when(u'o cliente pressiona o botão "proceed to checkout"')
def step_impl(context):
    context.btn_proceed_to_checkout.click()


@then(u'o cliente deve avançar para a etapa sign')
def step_impl(context):
    page_heading = wait(context.web, 20).until(EC.presence_of_element_located((By.CLASS_NAME, "page-heading")))
    assert page_heading.is_displayed()
    assert page_heading.text.lower() == "authentication"
