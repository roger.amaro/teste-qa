from behave import when, then, given
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait as wait


@given(u'que o cliente esta na pagina de validação do endereço')
def step_impl(context):
    address_delivery = wait(context.web, 10).until(EC.element_to_be_clickable((By.ID, "address_delivery")))
    assert address_delivery.is_displayed() is True


@given(u'que o cliente esta vendo a caixa de selecção dos termos de serviço da entrega')
def step_impl(context):
    context.check_box_terms_of_service = context.web.find_element_by_id("uniform-cgv")
    assert context.check_box_terms_of_service.is_displayed() is True


@when(u'clicar na caixa de seleção')
def step_impl(context):
    context.check_box_terms_of_service.click()


@then(u'a caixa de seleção deve estar marcada')
def step_impl(context):
    context.check_box_terms_of_service = context.web.find_element_by_id("cgv")
    assert context.check_box_terms_of_service.is_selected() is True
