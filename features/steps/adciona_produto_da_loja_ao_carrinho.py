from behave import when, then
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait as wait

base_url = "http://automationpractice.com/index.php?"


@when(u'o usuário adicionar qualquer produto a sacola')
def step_impl(context):
    try:
        btn = wait(context.web, 10).until(EC.element_to_be_clickable((By.LINK_TEXT, "Add to cart")))
        btn.click()
    except Exception as err:
        raise NotImplementedError(u'Botão não encontrado.')


@then(u'o usuário verá um resumo da sua sacola de compras')
def step_impl(context):

    btn = wait(context.web, 10).until(EC.visibility_of_element_located((By.CLASS_NAME, "clearfix")))
    assert btn.is_displayed() is True, "não foi exibido o resumo de itens no carrinho"
