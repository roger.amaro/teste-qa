from behave import when, then, given
from faker import Faker

f = Faker()


@given(u'que o cliente esta na etapa "sign-in"')
def step_impl(context):
    page_heading = context.web.find_element_by_class_name("page-heading")
    assert page_heading.is_displayed()
    assert page_heading.text.lower() == "authentication"


@when(u'o cliente preenche o campo "email address" com um email válido')
def step_impl(context):
    email_box = context.web.find_element_by_id("email_create")
    email_box.send_keys(f.email())


@when(u'clica no botão "create an account"')
def step_impl(context):
    ca_button = context.web.find_element_by_name("SubmitCreate")
    ca_button.click()


@then(u'o cliente verá a tela de cadastro com o formulário de cadastro')
def step_impl(context):

    submit_class = context.web.find_element_by_class_name("submit")
    assert submit_class.is_displayed() is True
