from behave import when, then, given
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait as wait


@given(u'que o cliente esta na página de confirmação do método de pagamento')
def step_impl(context):
    navigation_page = wait(context.web, 20).until(EC.presence_of_element_located((By.CLASS_NAME, "navigation_page")))
    assert navigation_page.is_displayed() is True
    assert navigation_page.text == "Bank-wire payment."


@when(u'o cliente clica no botão "I confirm my order"')
def step_impl(context):
    context.web.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    btn_confirm = [x for x in context.web.find_elements_by_tag_name("span") if x.text == "I confirm my order"]
    assert btn_confirm[0].is_displayed() is True
    btn_confirm[0].click()


@then(u'o cliente visualiza a pagina de confirmação do pedido')
def step_impl(context):
    navigation_page = wait(context.web, 20).until(EC.presence_of_element_located((By.CLASS_NAME, "navigation_page")))
    assert navigation_page.is_displayed() is True
    assert navigation_page.text == "Order confirmation"


@then(u'a mensagem "Your order on My Store is complete." estará visível')
def step_impl(context):
    msg_confirm = context.web.find_element_by_class_name("cheque-indent")
    assert "Your order on My Store is complete." == msg_confirm.text