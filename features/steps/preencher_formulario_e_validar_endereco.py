from behave import when, then, given
from faker import Faker
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait as wait
from random import randint

f = Faker()


@given(u'que o formulário de cadastro visível')
def step_impl(context):
    submit_class = context.web.find_element_by_class_name("submit")
    assert submit_class.is_displayed() is True


@when(u'o cliente preencher todos os dados do formulário corretamente')
def step_impl(context):
    radio_gender = wait(context.web, 10).until(EC.element_to_be_clickable((By.ID, "id_gender1")))
    radio_gender.click()

    first_name = wait(context.web, 10).until(EC.element_to_be_clickable((By.ID, "customer_firstname")))
    last_name = wait(context.web, 10).until(EC.element_to_be_clickable((By.ID, "customer_lastname")))

    full_name = f.name().split(" ")
    first_name.send_keys(full_name[0])
    last_name.send_keys(full_name[1])
    context.name = full_name

    company = wait(context.web, 10).until(EC.element_to_be_clickable((By.ID, "company")))

    context.company = f.company()
    company.send_keys(context.company)

    address1 = wait(context.web, 10).until(EC.element_to_be_clickable((By.ID, "address1")))
    context.address1 = f.address()
    address1.send_keys(context.address1)

    city = wait(context.web, 10).until(EC.element_to_be_clickable((By.ID, "city")))
    context.city = f.city()
    city.send_keys(context.city)

    postcode = wait(context.web, 10).until(EC.element_to_be_clickable((By.ID, "postcode")))

    context.postcode = f.postcode()
    postcode.send_keys(context.postcode)

    phone_mobile = wait(context.web, 10).until(EC.element_to_be_clickable((By.ID, "phone_mobile")))
    context.phone_mobile = randint(1000000, 90000000)
    phone_mobile.send_keys(context.phone_mobile)

    address_alias = wait(context.web, 10).until(EC.element_to_be_clickable((By.ID, "alias")))
    context.address_alias = f.secondary_address()
    address_alias.send_keys(context.address_alias)

    state = f.state()
    context.web.find_element_by_xpath(f"//select[@name='id_state']/option[text()='{state}']").click()
    context.state = state
    passwd = wait(context.web, 10).until(EC.element_to_be_clickable((By.ID, "passwd")))
    passwd.send_keys(2638726387)


@when(u'clicar no botão "Register"')
def step_impl(context):
    btn_send = wait(context.web, 10).until(EC.element_to_be_clickable((By.ID, "submitAccount")))
    btn_send.click()


@then(u'os dados sao verificados')
def step_impl(context):
    address_delivery = wait(context.web, 10).until(EC.element_to_be_clickable((By.ID, "address_delivery")))
    assert address_delivery.is_displayed() is True
    full_name = context.web.find_element_by_class_name("address_firstname").text
    assert full_name.split(" ") == context.name
    company = context.web.find_element_by_class_name("address_company").text
    assert company == context.company
    address = context.web.find_element_by_class_name("address_address1").text
    assert address == context.address1.split("\n")[0]
    address_city = context.web.find_element_by_class_name("address_city").text
    assert context.city in address_city
    assert context.state in address_city
    address_phone_mobile = context.web.find_element_by_class_name("address_phone_mobile").text
    assert int(address_phone_mobile) == context.phone_mobile