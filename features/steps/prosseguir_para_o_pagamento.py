from behave import given, when, then
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait as wait


@given(u'que o cliente esteja no etapa de termos de serviço de entrega')
def step_impl(context):
    navigation_page = context.web.find_element_by_class_name("navigation_page")
    assert navigation_page.is_displayed() is True
    assert navigation_page.text == 'Shipping'


@given(u'o botão de "proceed to checkout" esta visível')
def step_impl(context):
    btn_processCarrier = context.web.find_element_by_name("processCarrier")
    assert btn_processCarrier.is_displayed() is True
    context.btn_processCarrier = btn_processCarrier


@when(u'o cliente clica no botão "proceed to checkout"')
def step_impl(context):
    context.btn_processCarrier.click()


@then(u'o cliente deve avançar para a etapa de pagamento')
def step_impl(context):
    navigation_page = wait(context.web, 20).until(EC.presence_of_element_located((By.CLASS_NAME, "navigation_page")))
    assert navigation_page.text == "Your payment method"
