from behave import when, then
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait as wait


@then(u'a página de validação do endereço é visível')
def step_impl(context):
    address_delivery = wait(context.web, 10).until(EC.element_to_be_clickable((By.ID, "address_delivery")))
    assert address_delivery.is_displayed() is True


@when(u'quando o botão "proceed to checkout" e pressionado')
def step_impl(context):
    btn_ptcheckout = context.web.find_element_by_name("processAddress")
    btn_ptcheckout.click()


@then(u'O usuário e direcionado para a etapa de termos de serviço de entrega')
def step_impl(context):
    navigation_page = wait(context.web, 20).until(EC.presence_of_element_located((By.CLASS_NAME, "navigation_page")))
    assert navigation_page.is_displayed() is True
    assert navigation_page.text == 'Shipping'
