from behave import when, then, given
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait as wait


@given(u'que o cliente esta visualizando a etapa de pagamento e os 2 métodos de pagamentos disponíveis')
def step_impl(context):
    navigation_page = wait(context.web, 20).until(EC.presence_of_element_located((By.CLASS_NAME, "navigation_page")))
    assert navigation_page.text == "Your payment method"
    bankwire = context.web.find_element_by_class_name("bankwire")
    assert bankwire.is_displayed() is True
    cheque = context.web.find_element_by_class_name("cheque")
    assert cheque.is_displayed() is True
    context.bankwire = bankwire


@when(u'o cliente clica em um método')
def step_impl(context):
    context.bankwire.click()


@then(u'o cliente é redirecionado para a confirmação do método')
def step_impl(context):
    navigation_page = wait(context.web, 20).until(EC.presence_of_element_located((By.CLASS_NAME, "navigation_page")))
    assert navigation_page.is_displayed() is True
