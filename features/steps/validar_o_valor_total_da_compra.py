from behave import given, then


@given(u'que o cliente esta visualizando a etapa de pagamento')
def step_impl(context):
    navigation_page = context.web.find_element_by_class_name("navigation_page")
    assert navigation_page.text == "Your payment method"


@then(u'deverá estar visível para o cliente o valor total da compra')
def step_impl(context):
    total_product = context.web.find_element_by_id("total_product").text[1:-1]
    total_price = context.web.find_element_by_id("total_price").text[1:-1]
    total_shipping = context.web.find_element_by_id("total_shipping").text[1:-1]
    assert float(total_product) + float(total_shipping) == float(total_price)
